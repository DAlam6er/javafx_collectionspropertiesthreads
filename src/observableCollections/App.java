package observableCollections;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import java.util.ArrayList;

public class App
{
    public static void main(String[] args)
    {
        // Постановка задачи: при добавлении и удалении элементов из коллекции
        // JavaFX, менялась коллекция старая
        ArrayList<Person> oldList = new ArrayList<>();
        oldList.add (new Person("Name1", 20));
        oldList.add (new Person("Name2", 30));

        // создаем лист на основе старого, в результате в новой коллекции
        // все что есть в старой
        // старую коллекцию передали в новую по ссылке, так что
        // те элементы, которые хранятся в старом списке,
        // в новом списке будут хранится по ссылке
        ObservableList<Person> newList =
            FXCollections.observableArrayList(oldList);

        // Прописываем как привязать новую коллекцию к старой
        // должно быть в отдельном классе
        newList.addListener(new ListChangeListener<Person>()
        {
            // Вешаем систему оповещений на коллекцию
            // когда меняется содержимое коллекции (добавление, удаление)
            // не сами элементы коллекции!
            // c - список списков изменений
            // то есть большой список, содержащий в себе
            // маленькие списки добавлений и удалений
            @Override
            public void onChanged(Change<? extends Person> c)
            {
                while(c.next()) { // если есть ещё куда идти дальше в списке
                    if (c.wasAdded()) {
                        //.getAddedSubList() возращает подсписок добавленных в c элементов
                        for (Person person : c.getAddedSubList()) {
                            oldList.add(new Person(person.name, person.age));
                            System.out.println("Added: " + person);
                        }
                    } else if (c.wasRemoved()) {
                        //.getRemoved() возращает подсписок удаленных элементов c
                        for (Person person : c.getRemoved()) {
                            int i = 0;
                            // в реальной БД надо искать по индексу
                            for (Person p: oldList ) {
                                if (p.name.equals(person.name)) {
                                    break;
                                }
                                i++;
                            }
                            // В старом списке человек, удаленный из нового списка не найден!
                            if (i == oldList.size()) {
                                System.out.println("Person was not found");
                                break;
                            }
                            oldList.remove(i);
                            System.out.println("Removed: " + person);
                        }
                    }
                }
            }
        });

        newList.add(new Person("Name3", 40));
        newList.remove(0);
        newList.get(0).age++; // человек с бывшим индексом (1) хранится в новом списке по ссылке, поэтому в старом списке его возраст тоже изменится!
        newList.get(1).age++; // человек с бывшим индексом (2) хранится в новом списке по значению, поэтому в старом списке его возраст НЕ ИЗМЕНИТСЯ!

        System.out.println("Old collection:");
        for (Person person : oldList) {
            System.out.println(person);
        }
    }

}

class Person
{
    String name; // видимость в рамках package
    int age;

    public Person(String name, int age)
    {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString()
    {
        return "Person{" + "name=" + name + ", age=" + age + '}';
    }
}

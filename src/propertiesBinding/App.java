package propertiesBinding;

import javafx.beans.property.DoubleProperty; // bean - какой-то небольшой класс, основа, фундамент библиотек
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class App
{
    public static void main(String[] args)
    {
        Person pers = new Person();
        pers.setHeight(10.5); // как бы я не обращался к pers, я всегда узнаю, что у него изменился рост

        DemoProperty demo = new DemoProperty();
        // привяжем свойство height К свойству k
        // делаем привязку в классе Person, а не в main
        // т.к. нет метода, который бы возращал height как DoubleProperty
        // k будет меняться когда height изменится
        pers.bindK(demo.k);
        System.out.println("k = " + demo.k.get());

        pers.setHeight(5.);
        System.out.println("k = " + demo.k.get());
        // при этом, если изменить k, то height меняться не будет!
    }
}

class Person
{
    private DoubleProperty height = new SimpleDoubleProperty(); // готовая реализация с настройками по умолчанию

    // при создании Person в КОНСТРУКТОРЕ будем создавать его с Listener'ом
    public Person()
    {
        // сначала вызываем InvalidationListener и делаем проверку,
        // если всё ок - вызываем ChangeListener
        height.addListener(new ChangeListener<Number>() // анонимный класс
        {
            @Override
            public void changed(ObservableValue<? extends Number> observable,
                                Number oldValue, Number newValue)
            {
                System.out.println("height, oldValue: " + oldValue +
                    " newValue: " + newValue);
            }
        });
    }

    public double getHeight()
    {
        return height.get();
    }

    public void setHeight(double height)
    {
        this.height.set(height);
    }

    public void bindK(DoubleProperty k)
    {
        // делаем привязку в классе Person, а не в main
        // т.к. нет метода, который бы возращал height как DoubleProperty
        // k будет меняться когда height изменится
        k.bind(height);
    }
}

class DemoProperty
{
    DoubleProperty k = new SimpleDoubleProperty(); // при изменении свойства height меняется коэффициент k
}
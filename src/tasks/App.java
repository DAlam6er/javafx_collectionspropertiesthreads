package tasks;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.concurrent.Task;
import javafx.stage.Stage;

public class App extends Application // превращаем в JavaFX приложение
{
    public static void main(String[] args) throws InterruptedException
    {
        MyTask task = new MyTask();
        // В реальной жизни Thread уже не нужен после того как task запущен
        // нужен только для команды start()
        // но может пригодиться
        // то есть
        //new Thread(task.start); // не создавая ссылки
        Thread thread = new Thread(task);
        //thread.setDaemon(true); // зависнет всё равно
        thread.start(); // вызывать напрямую call() нельзя -
        // это все равно что run() вызывать - это обычный метод
        // все остальные действия с task ...
        // Вариант завершения System.exit(0); жёстко!
        DoubleProperty status = new SimpleDoubleProperty();
        status.bind(task.progressProperty());
//      while(task.isRunning()) { // так писать нельзя, это только изнутри потока, он стартует какое-то время. Для внутренних проверок
        while(!task.isDone()) { // ПОКА поток НЕ завершился
            Thread.sleep(1000); // опрашиваем поток с определенной периодичностью в 1 с
            // слишком часто нельзя опрашивать процесс
            // не всегда известно сколько выполняется один шаг потока (там может быть сложная математика)
            System.out.println(status.get());
        }
        Platform.exit();
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        // не нужен, т.к. всё в main работает
    }
}

class MyTask extends Task<Void>
{
    @Override
    protected Void call() throws Exception // Callable интерфейс наследник Runnable
    {
        System.out.println(Thread.currentThread().getName() + " - started!");
        for (int i = 0; i < 10; i++) {
            Thread.sleep(500); // будет работать 5 000 единиц времени
            updateProgress(i+1,10); // предельное значение с потолка
        }
        return null;
    }

    @Override
    protected void succeeded()
    {
        System.out.println("Task finished!"); // поток завершил свою работу
    }
}
